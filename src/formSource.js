export const userInputs = [
  {
    id: "carname",
    label: "Car Name",
    type: "text",
    placeholder: "Daihatsu Sigra",
  },
  {
    id: "price",
    label: "Price",
    type: "text",
    placeholder: "Rp. xxx.xxx,00",
  },
  {
    id: "engine",
    label: "Engine",
    type: "text",
    placeholder: "Manual or Automatic",
  },
  {
    id: "startrent",
    label: "Start Rent",
    type: "text",
    placeholder: "from day",
  },
  {
    id: "endrent",
    label: "End Rent",
    type: "text",
    placeholder: "to day",
  },
  {
    id: "updated",
    label: "Updated",
    type: "text",
    placeholder: "ex. Wednesday 14-10, 2021",
  },
  // {
  //   id: "displayName",
  //   label: "Name and surname",
  //   type: "text",
  //   placeholder: "John Doe",
  // },
  // {
  //   id: "email",
  //   label: "Email",
  //   type: "mail",
  //   placeholder: "john_doe@gmail.com",
  // },
  // {
  //   id: "phone",
  //   label: "Phone",
  //   type: "text",
  //   placeholder: "+1 234 567 89",
  // },
  // {
  //   id: "password",
  //   label: "Password",
  //   type: "password",
  // },
  // {
  //   id: "address",
  //   label: "Address",
  //   type: "text",
  //   placeholder: "Elton St. 216 NewYork",
  // },
  // {
  //   id: "country",
  //   label: "Country",
  //   type: "text",
  //   placeholder: "USA",
  // },
];

export const productInputs = [
  {
    id: 1,
    label: "Title",
    type: "text",
    placeholder: "Apple Macbook Pro",
  },
  {
    id: 2,
    label: "Description",
    type: "text",
    placeholder: "Description",
  },
  {
    id: 3,
    label: "Category",
    type: "text",
    placeholder: "Computers",
  },
  {
    id: 4,
    label: "Price",
    type: "text",
    placeholder: "100",
  },
  {
    id: 5,
    label: "Stock",
    type: "text",
    placeholder: "in stock",
  },
];
