import { StopTwoTone } from "@mui/icons-material";
import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
    name: "user",
    initialState:{
        
  loading: false,
  hasErrors: false,
  user: [],
  button:''
    },
    reducers: {
        getuser: state => {
          state.loading = true
        },
        getuserSuccess: (state, { payload }) => {
          state.user = payload
          state.loading = false
          state.hasErrors = false
        },
        getuserFailure: state => {
          state.loading = false
          state.hasErrors = true
        },
        getButton:(state,{payload}) =>{
          state.button = payload
        }
      },
})

export const { getuser, getuserSuccess, getuserFailure, getButton } = userSlice.actions
export default userSlice.reducer
export const userSelector = state => state.user

export function fetchuser() {
    return async dispatch => {
      dispatch(getuser())
  
      try {
        const response = await fetch('https://625d14fe4c36c753576e928d.mockapi.io/item')
        const data = await response.json()
        dispatch(getButton('pilih'))
        dispatch(getuserSuccess(data))
      } catch (error) {
        dispatch(getuserFailure())
      }
    }
  }

  export function fetchuserdetail(id) {
    return async dispatch => {
      dispatch(getuser())
  
      try {
        const response = await fetch(`https://625d14fe4c36c753576e928d.mockapi.io/item/${id}`)
        const data = await response.json()
        dispatch(getButton('Rent Now'))
        dispatch(getuserSuccess(data))
      } catch (error) {
        dispatch(getuserFailure())
      }
    }
  }