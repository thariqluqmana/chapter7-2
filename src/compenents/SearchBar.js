import React, { useEffect, useState } from 'react'
import Box from '@mui/material/Box';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import TimePicker from '@mui/lab/TimePicker';
import Button from '@mui/material/Button';
import DatePicker from '@mui/lab/DatePicker';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import './SearchBar.css'

import LocalizationProvider from '@mui/lab/LocalizationProvider';
import axios from 'axios';
const currencies = [
    {
      value: '3',
      label: 'Xenia',
    },
    {
      value: '4',
      label: 'Avanza',
    },
    {
      value: '2',
      label: 'APV',
    },
    {
      value: '7',
      label: 'Ferrari',
    },
    {
      value: '5',
      label: 'BMW X1',
    }
  ];
function SearchBar() {
    const [currency, setCurrency] = React.useState('...');
    const [value, setValue] = React.useState(null);
    const [value1, setValue1] = React.useState(null);
    const [dataCar,setDataCar] = useState([])
    const handleChange = (event) => {
      setCurrency(event.target.value);
    };
    const handleChange1 = (event) => {
        setCurrency(event.target.value1);
      };
      useEffect(()=>{
        axios.get('https://rent-cars-api.herokuapp.com/admin/car').then((res) => {
          console.log(res.data)
          setDataCar(res.data)
      })
    },[])

      const handleCard = (id)=>{
        console.log(id)
        window.location.href = `/home/result/${id}`
      }
  return (
    <div className='SearchBar'>
         <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          p: 1,
          m: 1,
          bgcolor: 'background.paper',
          borderRadius: 1
        }}
        
      >
        
        <TextField
          id="outlined-select-currency"
          select
          label="Select"
          value={currency}
          onChange={handleChange}
          helperText="Please select your currency"
        >
          {currencies.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>

<LocalizationProvider dateAdapter={AdapterDateFns}>
      <TimePicker
        label="Basic example"
        value={value}
        onChange={(newValue) => {
          setValue(newValue);
        }}
        renderInput={(params) => <TextField {...params} />}
      
      />
    </LocalizationProvider>
    <LocalizationProvider dateAdapter={AdapterDateFns}>
  <DatePicker
    label="Basic example"
    value={value1}
    onChange={(newValue) => {
      setValue1(newValue);
    }}
    renderInput={(params) => <TextField {...params} />}
  />
</LocalizationProvider>
      <TextField
          id="outlined-number"
          label="Number"
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
        />

        <Button variant="contained" size='medium' color="success" onClick={()=>handleCard(currency)} >Search</Button>

      </Box>
    </div>
  )
}

export default SearchBar