import React, { useContext, useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import { Link, Navigate, useNavigate } from "react-router-dom";
import GoogleIcon from "@mui/icons-material/Google";
import GitHubIcon from "@mui/icons-material/GitHub";

import { auth } from "../firebase";
import { signInWithEmailAndPassword } from "firebase/auth";
import "./logins.scss";
import axios from "axios";
import { data } from "../App";
import { AuthContext } from "../Context/AuthContext";
const Logins = () => {
  // const { user, setUser } = useContext(data);
  // const [username, setUsername] = useState();
  // const [password, setPassword] = useState();

  // const handleSubmit = async (e) => {
  //   e.preventDefault();
  //   try {
  //     const res = await axios.post("/login", { username, password });
  //     setUser(res.data);
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  // const google = () => {
  //   window.open("http://localhost:5000/auth/google", "_self");
  // };
  // const github = () => {
  //   window.open("http://localhost:5000/auth/github", "_self");
  // };
  const [error, setError] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navitage = useNavigate();
  const { dispatch } = useContext(AuthContext);
  const handleLogin = (e) => {
    e.preventDefault();

    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        dispatch({ type: "LOGIN", payload: user });
        navitage("/");
      })
      .catch((error) => {
        setError(true);
      });
  };
  return (
    <div className="Logins">
      <Card sx={{ maxWidth: 345, width: 345 }}>
        <CardMedia
          component="img"
          height="200"
          image="images/sandtruck.png"
          alt="cupang"
        />
        <CardContent>
          <Typography
            sx={{ mt: 3, fontFamily: "Montserrat" }}
            gutterBottom
            variant="h5"
            component="div"
          >
            Welcome to Jubil
          </Typography>
          {error && <span className="errorMessage">name pass salah!</span>}
          <Typography variant="body2" color="text.secondary">
            <form onSubmit={handleLogin}>
              <FormControl fullWidth variant="filled">
                <TextField
                  sx={{ mt: 3 }}
                  id="standard-basic"
                  label="Username"
                  variant="outlined"
                  role={"form"}
                  name="username"
                  onChange={(e) => setEmail(e.target.value)}
                />
                <TextField
                  sx={{ mt: 1 }}
                  type="password"
                  id="standard-basic"
                  label="Password"
                  variant="outlined"
                  role={"formOne"}
                  name="password"
                  onChange={(e) => setPassword(e.target.value)}
                />
                <Button
                  type="submit"
                  sx={{ mt: 1 }}
                  role={"Button"}
                  variant="contained"
                >
                  Login
                </Button>
              </FormControl>
            </form>
          </Typography>
        </CardContent>
        {/* Router */}

        <Button
          role={"Button"}
          sx={{ ml: 2, mb: 3 }}
          variant="contained"
          // onClick={google}
        >
          <GoogleIcon />
        </Button>
        <Button
          role={"Button"}
          sx={{ ml: 2, mb: 3 }}
          variant="contained"
          // onClick={github}
        >
          <GitHubIcon />
        </Button>
      </Card>
      {/* <ComponentWithUseState/> */}
    </div>
  );
};

export default Logins;
