import React from 'react'

import Navbar from './Navbar';
import ContentSC from './ContentSC';
import Footer from './Footer';
import SearchBar from './SearchBar';



function SearchCar() {
    
  return (
    <div className='SearchCar'>
    <Navbar />
    <ContentSC />
    <SearchBar/>
    <Footer/>
    
    </div>
  )
}

export default SearchCar