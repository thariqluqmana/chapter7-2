import { Card } from '@mui/material'
import React from 'react'
import Footer from './Footer'
import Navbar from './Navbar'
import Cards from './Cards'
import Grid from '@mui/material/Grid';
import SearchBar from './SearchBar'
function ResultSC() {
  
  return (
    <div className='ResultSC'>
        <Navbar/>
        <SearchBar/>
        <Grid container spacing={4} sx={{
        display:'flex', justifyContent:'center', paddingTop:'50px'
      }}>
 
  <Cards/>
  
</Grid>
        
        <Footer/>
    </div>
  )
}

export default ResultSC