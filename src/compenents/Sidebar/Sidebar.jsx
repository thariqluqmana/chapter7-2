import React from "react";
import "./sidebar.scss";
import DashboardIcon from "@mui/icons-material/Dashboard";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import KeyboardTabIcon from "@mui/icons-material/KeyboardTab";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import QueryStatsIcon from "@mui/icons-material/QueryStats";
import NotificationsActiveIcon from "@mui/icons-material/NotificationsActive";
import HealthAndSafetyIcon from "@mui/icons-material/HealthAndSafety";
import PsychologyIcon from "@mui/icons-material/Psychology";
import SettingsIcon from "@mui/icons-material/Settings";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import LogoutIcon from "@mui/icons-material/Logout";
import { Link } from "react-router-dom";

const Sidebar = () => {
  return (
    <div className="Sidebar">
      <div className="top">
        <span className="logo">Titodmin</span>
      </div>
      <hr />
      <div className="center">
        <ul>
          <p className="title">Dashboard</p>
          <li>
            <Link to="/" style={{ textDecoration: "none" }}>
              <span>
                <DashboardIcon className="icon" />{" "}
                <span className="listIcon">Dashboard</span>
              </span>
            </Link>
          </li>
          <p className="title">LISTS</p>
          <li>
            <Link to="/listcardashboard" style={{ textDecoration: "none" }}>
              <span>
                <PersonOutlineIcon className="icon" />{" "}
                <span className="listIcon">ListCar</span>
              </span>
            </Link>
          </li>
        </ul>
      </div>
      <div className="bottom">
        <div className="colorOption"></div>
        <div className="colorOption"></div>
      </div>
    </div>
  );
};

export default Sidebar;
