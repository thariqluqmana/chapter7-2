import React from 'react'
import CardsDetail from './CardsDetail'
import CardsDetail2 from './CardsDetail2'
import Footer from './Footer'
import Navbar from './Navbar'
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import SearchBar from './SearchBar'
import { useParams } from 'react-router-dom'

function DetailCarPage() {
  
  return (
    <div className='DetailCarPage'>
        <Navbar/>
        <SearchBar/>
        
        <Box >
      <Grid  container spacing={0}sx={{ flexGrow: 1, display: 'flex', justifyContent:'center', alignSelf:'center', px:'100px'}}  >
        <Grid item xs={8} sx={{ flexGrow: 1, display: 'flex', justifyContent:'center', alignSelf:'center',flexDirection:'column', px:'100px'  }}>
        <CardsDetail/>
        </Grid>
        <Grid item xs={4}>
        <CardsDetail2/>
        </Grid>
        
      </Grid>
    </Box>
        <Footer/>
    </div>
  )
}

export default DetailCarPage