import React, { useEffect, useState } from 'react'
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import CardMedia from '@mui/material/CardMedia';
import { FiUsers, FiSettings, FiCalendar } from 'react-icons/fi'
import Grid from '@mui/material/Grid';
import axios from 'axios'
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { fetchuser, getButton, userSelector } from '../redux/userSlice';

  
const Cards = () => {
  const { user, loading, hasErrors, button } = useSelector(userSelector)
  console.log(user);
  const dispatch = useDispatch()
  const {id}= useParams()
  console.log(id);
  const [dataCar,setDataCar] = useState([])
  const handleCard = (id)=>{
    console.log(id)
    window.location.href = `/home/result/detail/${id}`
  }
  useEffect(()=>{
    axios.get(`https://rent-cars-api.herokuapp.com/admin/car/${id}`).then((res) => {
      console.log(res.data)
      setDataCar(res.data)
  })
},[])
useEffect(() => {
  dispatch(fetchuser(),getButton('choose'))
}, [dispatch])





const array = [['hellow','pastas'],['travel', 'militaries'],['oranges','mint']]

const arrayOne = array.flat(1).filter(e=> e.length > 6 )

console.log(arrayOne)

const renderCard = () => {
  if (loading) return <p>Loading recipes...</p>
  if (hasErrors) return <p>Cannot display recipes...</p>

  return user.map(users =>
    // <div key={users.idMeal} className='tile'>
    //   <h2>{users.strMeal}</h2>
    //   <img src={users.strMealThumb} alt=''/>
    // </div>
     <div className='Cards' key={users.id}>
         
     <Box sx={{ minWidth: 275 }}>
     <Grid item xs={4} >
   <Card variant="outlined" sx={{width:'400px'}}>
   <CardMedia
     component="img"
     height="260"
     image={users.image}
     alt="trek pasir"
   />
   <CardContent>
   <Typography variant="h5" component="div">
       {users.name}
     </Typography>
     <Typography variant="p" component="div">
       Rp. {users.price}
     </Typography>
     <Typography variant="p" component="div">
       <FiUsers/> {users.passenger}
     </Typography>
     <Typography variant="p" component="div">
       <FiSettings/> {users.model}
     </Typography>
     <Typography variant="p" component="div">
       <FiCalendar/> {users.time}
       {console.log(users.time)}
     </Typography>
     
   </CardContent>
   <CardActions>
     <Button size="small" variant='contained' onClick={()=>handleCard(users.id)}>{button}</Button>
   </CardActions>
   </Card>
   </Grid>
 </Box>
 </div>
  )
}
  

  return (
   renderCard()
    
  )

}

export default Cards