import { Typography } from '@mui/material'
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import React from 'react'
import { IoIosAddCircle,IoIosBeer,IoIosBulb,IoIosCopy,IoIosCar } from "react-icons/io";

function Footer() {
  return (
    <div className='Footer'>
        
<Box >
      <Grid container spacing={5} sx={{
        display:'flex', justifyContent:'center', paddingTop:'50px'
      }}>
  <Grid item xs={2}>
  <Typography sx={{marginBottom:'7px'}}>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</Typography>
  <Typography sx={{marginBottom:'7px'}}>binarcarrental@gmail.com</Typography>
  <Typography sx={{marginBottom:'7px'}}>081-233-334-808</Typography>
  </Grid>
  <Grid item xs={2}>
  <Typography sx={{marginBottom:'7px', fontWeight:'bold'}}>Our Services</Typography>
  <Typography sx={{marginBottom:'7px', fontWeight:'bold'}}>Why Us</Typography>
  <Typography sx={{marginBottom:'7px', fontWeight:'bold'}}>Testimonial</Typography>
  <Typography sx={{marginBottom:'7px', fontWeight:'bold'}}>FAQ</Typography>
  </Grid>
  <Grid item xs={2}>
  <Typography>
    <Box sx={{display:'flex', fontSize:'24px'}}>
      <IoIosAddCircle/>   
      <IoIosBeer/>   
      <IoIosBulb/>   
      <IoIosCopy/>   
    </Box>
  </Typography>
  </Grid>
  <Grid item xs={2}>
    <Typography>Copyright Binar</Typography>
    <Box sx={{display:'flex', fontSize:'90px', color:'green'}}>
    <IoIosCar/>
    </Box>
  </Grid>
</Grid>
    </Box>
    </div>
  )
}

export default Footer