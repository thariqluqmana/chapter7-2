import React, { useEffect, useState } from "react";
import "./listCarDashboard.scss";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import CardMedia from "@mui/material/CardMedia";
import { FiUsers, FiSettings, FiCalendar } from "react-icons/fi";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import Grid from "@mui/material/Grid";
import Sidebar from "./Sidebar/Sidebar";
import Navbar from "./Navbar/Navbar";
import { Link } from "react-router-dom";
import { collection, getDocs, doc, deleteDoc } from "firebase/firestore";
import { db } from "../firebase";
import { Alert } from "@mui/material";

const ListCarDashboard = () => {
  //
  const [data, setData] = useState([]);
  const [flash, setFlash] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      let list = [];
      try {
        const querySnapshot = await getDocs(collection(db, "cars"));
        querySnapshot.forEach((doc) => {
          list.push({ id: doc.id, ...doc.data() });
        });

        setData(list);
        console.log(list);
      } catch (err) {
        console.log(err);
      }
    };
    fetchData();
  }, []);

  const handleDelete = async (id) => {
    try {
      await deleteDoc(doc(db, "cars", id));
      setData(data.filter((item) => item.id !== id));
    } catch (err) {
      console.log(err);
    }
  };

  const renderCard = (card, index) => {
    return (
      <Box sx={{ minWidth: 275 }} className="card">
        <Grid item xs={4}>
          <Card variant="outlined" sx={{ width: "400px" }} key={index}>
            <CardMedia
              component="img"
              height="260"
              image={card.img}
              alt="trek pasir"
            />
            <CardContent>
              <Typography variant="h5" component="div">
                {card.carname}
              </Typography>
              <Typography variant="p" component="div">
                Rp. {card.price}
              </Typography>
              <Typography variant="p" component="div">
                <FiUsers /> started {card.startrent}
              </Typography>
              <Typography variant="p" component="div">
                <FiSettings /> {card.engine}
              </Typography>
              <Typography variant="p" component="div">
                <FiCalendar />
                ended at {card.endrent}
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" variant="contained" color="success">
                Edit
              </Button>
              <Button
                size="small"
                variant="contained"
                color="error"
                onClick={() => handleDelete(card.id)}
              >
                Delete
              </Button>
            </CardActions>
          </Card>
        </Grid>
      </Box>
    );
  };
  return (
    <div className="listCarDashboard">
      <Sidebar />
      <div className="homeContainer">
        <Navbar />
        {/* {flash && (
          <Alert severity="success">
            This is a success alert — check it out!
          </Alert>
        )} */}
        <Link to="/adduser" style={{ textDecoration: "none" }}>
          <div className="button" style={{ paddingLeft: 30, paddingTop: 20 }}>
            <Button size="small" variant="contained" color="success">
              <AddCircleIcon />
            </Button>
          </div>
        </Link>
        <div className="listcar">{data.map(renderCard)}</div>
      </div>
    </div>
  );
};

export default ListCarDashboard;
