import { TextField, Typography } from "@mui/material";
import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import "@testing-library/jest-dom";
import Logins from "./Logins.js";

test("Welcome to Jubil title test", () => {
  render(<Logins />, { wrapper: BrowserRouter });
  const title = screen.getByText(/Welcome to Jubil/i);
  expect(title).toBeInTheDocument();
});

test("renders textField", () => {
  render(<Logins />, { wrapper: BrowserRouter });
  const form = screen.getByRole("form");
  expect(form).toBeInTheDocument();
});
test("renders Second textField", () => {
  render(<Logins />, { wrapper: BrowserRouter });
  const form = screen.getByRole("formOne");
  expect(form).toBeInTheDocument();
});

test("renders Button", () => {
  render(<Logins />, { wrapper: BrowserRouter });
  const Button = screen.getByRole("Button");
  expect(Button).toBeInTheDocument();
});
